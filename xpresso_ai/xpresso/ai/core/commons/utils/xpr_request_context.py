""" Request Object for API calls """

__all__ = ["XprRequestContext"]
__author__ = "Mrunalini Dhapodkar"

import threading
from xpresso.ai.core.commons.utils.singleton import Singleton


class XprRequestContext(metaclass=Singleton):
    """
    Defines an Instance operation that lets logger access
    request ID for every API call wrt current thread ID.
    """
    def __init__(self):
        super().__init__()
        self.thread_request_map = {}

    def store_request_id(self, request_id):
        """

        Maps request ID to current thread ID and stores this mapping
        Args:
            request_id: Request ID for API call

        """
        thread_id = threading.get_ident()
        self.thread_request_map[thread_id] = request_id

    def return_request_id(self):
        """

        Returns: request ID wrt current thread ID for logging

        """
        if not self.thread_request_map:
            return None
        thread_id = threading.get_ident()
        return self.thread_request_map[thread_id]
