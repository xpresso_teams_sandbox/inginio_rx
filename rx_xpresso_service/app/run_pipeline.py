import datetime
import json
import os
import re

import pytz
import requests
from config import rx_config


class Submit:
    def __init__(self, filename):
        self.mount_path = rx_config.MOUNT_PATH
        self.config_file = json.loads(open(os.path.join(self.mount_path, 'config.json')).read())
        self.project_name = self.config_file['project_name']
        self.pipeline_name = self.config_file['pipeline_name']
        self.pipeline_version = self.config_file['pipeline_version']
        self.filename = filename

        # Server constants
        self.TIME_OUT = 3600
        self.token = None
        self.run_name = None
        self.controller_url = "https://172.16.6.1:5050"
        self.login()

    def submit(self):
        print("Login succesfull starting pipeline run stage")
        if not self.run_experiment():
            return False
        return True

    def login(self):
        url = f"{self.controller_url}/auth"
        data = {"uid": "xprsu", "pwd": "Xpresso@Abz00ba2019#@!"}

        r = requests.post(url, json=data, verify=False)
        if r.json()['outcome'] == 'success':
            self.token = r.json()["results"]["access_token"]

    def run_experiment(self):
        tz_India = pytz.timezone("Asia/Calcutta")
        global_time = datetime.datetime.now(tz_India)
        filename = self.filename.split(".pdf")[0]
        filename = filename.replace(" ", "_")
        filename = re.sub('[^\w]', '', filename)
        filename = filename.replace("__", "_")
        self.run_name = f"{filename}_{global_time.strftime('%Y%m%d%H%M%S')}"
        start_experiment_json = self.get_start_experiment_json()

        url = f"{self.controller_url}/experiments/start"
        r = requests.post(url, headers={"token": self.token},
                          json=start_experiment_json, verify=False)

        if r.json()['outcome'] == 'success':
            print("Pipeline run request send successfully")
            return True
        return False

    def get_start_experiment_json(self):
        start_experiment_json = {
            "run_name": self.run_name,
            "project_name": self.project_name,
            "pipeline_name": self.pipeline_name,
            "pipeline_version": self.pipeline_version,
            "run_parameters": {
                "file_name": f"{self.filename}"
            }
        }

        return start_experiment_json
